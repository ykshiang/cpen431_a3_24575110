# A3

#####Port on server:
#127.0.0.1:5110 (planetlab2.cs.ubc.ca)
#127.0.0.1:5110 (cs-planetlab3.cs.surrey.sfu.ca)

####Student Name: Kang Shiang Yap
####Student ID: 24575110

#### Additional Error Code
#0x07 No value and value length
#0x08 Invalid value field size

### Description
This assignment is implemented using multiple classes. In the context of this assignment, my ChatServer is the server object that serves incoming UDP connection. PackageManager handles the protocol layer of the system and ApplicationManager manages the application layer of the system. To handle multiple incoming connections at the same time, threads are created. each thread takes care of a request and responds to the requests accordingly. A cache system is also used to handle duplicate incoming packages. 

### Executed Test
##### PUT->GET->REMOVE->GET
#
> The test returns "succeed", implying that the application is able to **store**, **retrieve**, **remove** a object successfully. 

##### PUT->PUT->GET
#
> The test returns "succeed", implying that the application is about to **update** a value successfully.  

##### PUT->REMOVE->REMOVE
#
> The test returns "succeed", implying that the application is about to **remove** a value successfully.  

##### Invalid command 
#
> The test returns "succeed", implying that the application is about to **"detect an invalid command"** successfully.  

##### Invalid value-length 
#
> The test returns "succeed", implying that the application is about to **"detect an invalid value-length"** successfully.  

##### Closed-loop client - PUT->GET->REMOVE
###### PUT
#
> The Put request in my test was able to reach 100.0% success rate, 0% error and takes 1 retries per request. All the requests have an average of 1.0ms latency. 
###### GET
#
> The GET request in my test was able to reach 100.0% success rate, 0% error and takes 1 retries per request. All the requests have an average of 1.0ms latency. 
###### REMOVE
#
> The REMOVE request in my test was able to reach 100.0% success rate, 0% error and takes 1 retries per request. All the requests have an average of 1.0ms latency. 

##### Closed-loop client (without retries) - PUT->GET->REMOVE
###### PUT
#
> The Put request in my test was able to reach 100.0% success rate, 0% error and takes 1 retries per request. All the requests have an average of 1.0ms latency. 
###### GET
#
> The GET request in my test was able to reach 99.0% success rate, 1% error and 0% timeout. All the requests have an average of 1.0ms latency. 
###### REMOVE
#
> The REMOVE request in my test was able to reach 99.0% success rate, 1% error and 0% timeout. All the requests have an average of 1.0ms latency. 

##### Out-of-space error
#
> My application store has a capacity of 1962 requests. My machine runs out of space as soon as my program received 14401 PUT requests.