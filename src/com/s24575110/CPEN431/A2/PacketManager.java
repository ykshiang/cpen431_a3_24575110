package com.s24575110.CPEN431.A2;

import com.matei.eece411.util.ByteOrder;

import java.net.*;

/**
 * Created by KangShiang on 2016-01-21.
 */
public class PacketManager implements Runnable{

    private final int headerSize = 16;
    private final int maxPayloadSize = 10035;

    private DatagramPacket packet;
    private DatagramSocket udpSocket;
    private byte[] packetData;

    /**
     * Constructor method.
     */
    public PacketManager(DatagramPacket dPack, DatagramSocket socket){
        this.packet = dPack;
        this.packetData = dPack.getData();
        this.udpSocket = socket;
    }

    /**
     * This methods handles the payload of the udp packet. It creates
     * an application with the payload. The payload is handled in the application layer.
     * @param data udp payload
     * @return payload to be returned to client.
     */
    private byte[] handlePayload(byte[] data) throws Exception {
        ApplicationManager appManager = new ApplicationManager(data);
        byte[] response = appManager.handleApplicationPayload(appManager.applicationPayload, this.packet.getLength() - 16);
        return response;
    }
    /**
     * This method responds to requests that has no duplicate in rlogs.
     * The executing thread store the response to rlogs and goes to sleep
     * for 5 seconds. When it wakes up, it deletes the entry
     *
     * @param response response to be return to clients
     * @exception java.io.IOException
     */
    private void responseToNonDulpicate(byte[] response) throws Exception{
        byte[] uniqueID = parseID(this.packetData);
        int packetSize = uniqueID.length + response.length;
        byte[] rPacket = new byte[packetSize];
        System.arraycopy(uniqueID,0,rPacket,0,uniqueID.length);
        System.arraycopy(response,0,rPacket,uniqueID.length,response.length);
        ChatServer.operateWithHashMap(uniqueID,response,0);

        InetAddress IPAddress = packet.getAddress();
        int port = packet.getPort();
        DatagramPacket sendPacket = new DatagramPacket(rPacket, rPacket.length, IPAddress, port);
        udpSocket.send(sendPacket);
        Thread.sleep(5000);
        ChatServer.operateWithHashMap(uniqueID,null,2);
    }

    /**
     * This method responds to requests that has a duplicate in rlogs.
     *
     * @param response response to be return to clients
     * @exception java.io.IOException
     */
    private void responseToDuplicate(byte[] response) throws Exception {
        byte[] uniqueID = parseID(this.packetData);
        int packetSize = uniqueID.length + response.length;
        byte[] rPacket = new byte[packetSize];
        System.arraycopy(uniqueID,0,rPacket,0,uniqueID.length);
        System.arraycopy(response,0,rPacket,uniqueID.length,response.length);

        InetAddress IPAddress = packet.getAddress();
        int port = packet.getPort();
        DatagramPacket sendPacket = new DatagramPacket(rPacket, rPacket.length, IPAddress, port);
        udpSocket.send(sendPacket);
    }

    /**
     * This method responds to requests depending if the requests
     * has been processed already.
     *
     * @param response response to be return to clients
     * @param  isDuplicate flag to indicate whether the request has
     *                     already been processed already.
     */
    private void respondToClient(byte[] response, boolean isDuplicate) throws Exception{

        if(isDuplicate){
            responseToDuplicate(response);
        }else{
            responseToNonDulpicate(response);
        }
    }

    /**
     * This method parses the packet and extracts the protocol unique ID
     * @param packet UDP packet that is being processed
     * @return the Unique Identifier in the form of byte array.
     */
    private byte[] parseID(byte[] packet) throws Exception{
        byte[] header = new byte[headerSize];
        System.arraycopy(packet,0,header,0,headerSize);
        return header;
    }

    /**
     * This method check if there is duplicated request given a unique
     * identifier.
     * @param uniqueID unique identifier of a udp packet.
     * @return true if duplicate exists and false otherwise.
     */
    private byte[] checkForDuplicateRequest(byte[] uniqueID){
        try {
            byte[] rslt = ChatServer.operateWithHashMap(uniqueID, null, 3);
            return rslt;
        }catch (NullPointerException e){
            return null;
        }
    }

    /**
     * This method handles the datagram by first parsing the unique identifier,
     * checks for duplication, and handles the packet accordingly.
     */
    public void handlePacket() throws Exception{
        try {
            byte[] uniqueID = parseID(this.packetData);
            //Check for duplicate unique ID here.
            byte[] response = checkForDuplicateRequest(uniqueID);
            if (response == null) {
                byte[] unTrimmedPayload = new byte[maxPayloadSize];
                //Process the payload here.
                System.arraycopy(this.packetData, headerSize, unTrimmedPayload, 0, maxPayloadSize);
                response = handlePayload(unTrimmedPayload);

                //Log the reply/response here.
                respondToClient(response, false);
            } else {
                respondToClient(response, true);
            }
        }catch(OutOfMemoryError e){
            System.out.println(e.toString());
            byte[] uniqueID = parseID(packetData);
            byte[] responseCode = new byte[4];
            byte[] responsePayload = new byte[1];

            ByteOrder.int2leb(2, responseCode, 0);
            System.arraycopy(responseCode, 0, responsePayload, 0, 1);

            byte[] rPacket = new byte[headerSize + responsePayload.length];
            System.arraycopy(uniqueID, 0, rPacket, 0, headerSize);
            System.arraycopy(responsePayload,0,rPacket,headerSize,responsePayload.length);

            InetAddress IPAddress = packet.getAddress();
            int port = packet.getPort();
            DatagramPacket sendPacket = new DatagramPacket(rPacket, rPacket.length, IPAddress, port);
            udpSocket.send(sendPacket);
        }
    }

    /**
     * This method calls handlePacket
     * @exception IllegalArgumentException IndexOutOfBoundsException
     */
    public void run(){
        try {
            handlePacket();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}

