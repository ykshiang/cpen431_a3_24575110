package com.s24575110.CPEN431.A2;

import com.matei.eece411.util.ByteOrder;
import com.matei.eece411.util.StringUtils;

import java.util.HashMap;

/**
 * Created by KangShiang on 2016-01-21.
 */
public class ApplicationManager{
    private static final int commandSize = 1;
    private static final int keySize = 32;
    private static final int valueLengthSize = 2;

    byte[] applicationPayload;
    private static HashMap kvStore = new HashMap();

    /**
     * This method simply deletes all the value in the hashmap.
     */
    private static void deleteAllValue(){
        kvStore.clear();
    }

    /**
     * This method simply stores the value in the hashmap given a key.
     * @param key key to the hashmap.
     * @param val val to be stored.
     * @return the val that was stored.
     */

    private static byte[] storeValueWithKey(String key, byte[] val){
        byte[] data = (byte[])kvStore.put(key, val);
        return data;
    }

    /**
     * This method simply stores the value in the hashmap given a key with
     * one guarantee: No duplicate!
     * @param key key to the hashmap.
     * @param val val to be stored.
     * @return the val that was stored.
     */
    private static byte[] storeValueWithKeyNoDup(byte[] key, byte[] val){
        String hexKey = StringUtils.byteArrayToHexString(key);
        byte[] value = (byte[])kvStore.get(hexKey);
        if(value == null){
            byte[] data = storeValueWithKey(hexKey, val);
            return data;
        }
        return null;
    }

    /**
     * This method simply update the value in the hashmap given a key.
     * @param key key to the hashmap.
     * @param val val to be stored.
     * @return the val that was updated.
     */
    private static byte[] putValueWithKey(byte[] key, byte[] val){
        String hexKey = StringUtils.byteArrayToHexString(key);
        byte[] data = (byte[])storeValueWithKey(hexKey,val);
        return data;
    }

    /**
     * This method deletes value from the hashmap given a key.
     * @param key key to the value that is going to be deleted.
     * @return the val data that was deleted.
     */
    private static byte[] deleteValueWithKey(byte[] key){
        String hexKey = StringUtils.byteArrayToHexString(key);
        byte[] data = (byte[])kvStore.remove(hexKey);
        return data;
    }

    /**
     * This method gets value from the hashmap given a key.
     * @param key key to the value that is going to be retrieved.
     * @return the val data that was retrieved.
     */
    private static byte[] getValueWithKey(byte[] key){
        String hexKey = StringUtils.byteArrayToHexString(key);
        byte[] data = (byte[])kvStore.get(hexKey);
        return data;
    }

    /**
     * This method perform transaction on rlogs. Atomicity is
     * guaranteed within this function.
     * @param mode mode of operation:
     *             0 : stores the value given a key and makes sure
     *                 duplicate does not exist
     *             1 : update the value for a given key. If the key
     *                 does not exists, it creates a new entry with the key
     *             2 : deletes the value for a given key. It returns null
     *                 if the key does not exist.
     *             3 : get the value given key.
     *             4 : delete all values in the kvStore
     * @param key key to the hashmap.
     * @param val val to be stored.
     * @return the val data that the operation was performed on.
     */
    public static synchronized byte[] operateWithHashMap(byte[] key, byte[] val, int mode){
        byte[] data=null;
        switch (mode) {
            case 0:
                data = storeValueWithKeyNoDup(key, val);
                return data;
            case 1:
                data = putValueWithKey(key, val);
                return data;
            case 2:
                data = deleteValueWithKey(key);
                return data;
            case 3:
                data = getValueWithKey(key);
                return data;
            case 4:
                deleteAllValue();
            default:
                break;
        }
        data = null;
        return data;
    }


    /**
     * Constructor method: Takes the payload data from the protocol layer and create an
     * application.
     * @param data
     */
    public ApplicationManager(byte[] data){
        this.applicationPayload = data;
    }

    /**
     * This method handles the application data.
     * @param data
     * @return the payload to be included
     */
    public static byte[] handleApplicationPayload(byte[] data, int payloadSize){
        byte[] CommandBytes = new byte[commandSize];
        byte[] keyBytes = new byte[keySize];
        byte[] valueLengthBytes = new byte[valueLengthSize];
        byte[] responseData = null;
        byte[] responseCode = new byte[4];
        byte[] sizeVal = new byte[4];

        System.arraycopy(data, 0, CommandBytes, 0, commandSize);
        System.arraycopy(data, commandSize, keyBytes, 0, keySize);
        System.arraycopy(data, commandSize + keySize, valueLengthBytes, 0, valueLengthSize);

        int command = CommandBytes[0];
        switch (command){
            case 1:
                System.out.println("Command:" + "Put");
                System.out.println("Expected Value Size :" + (payloadSize - 35));
                int expectedValueSize = payloadSize - 35;
                int size = ByteOrder.leb2int(valueLengthBytes,0,2);
                if(size <= 10000) {
                    if(expectedValueSize < 0){
                        byte[] responsePayload = new byte[1];
                        ByteOrder.int2leb(7, responseCode, 0);
                        System.arraycopy(responseCode, 0, responsePayload, 0, 1);
                        return responsePayload;
                    }
                    if(expectedValueSize < size){
                        byte[] responsePayload = new byte[1];
                        ByteOrder.int2leb(8, responseCode, 0);
                        System.arraycopy(responseCode, 0, responsePayload, 0, 1);
                        return responsePayload;
                    }
                    System.out.println("Value Size:" + size);
                    byte[] value = new byte[size];
                    System.arraycopy(data, commandSize + keySize + valueLengthSize, value, 0, size);
                    operateWithHashMap(keyBytes, value, 1);
                }else{
                    byte[] responsePayload = new byte[1];
                    ByteOrder.int2leb(6, responseCode, 0);
                    System.arraycopy(responseCode, 0, responsePayload, 0, 1);
                    return responsePayload;
                }
                break;
            case 2:
                System.out.println("Command:" + "Get");
                // Retrieve the data from hashmap
                responseData = operateWithHashMap(keyBytes, null, 3);
                if (responseData != null) {
                    byte[] responsePayload = new byte[1 + 2 + responseData.length];

                    // Generate the status code and put it in the payload
                    ByteOrder.int2leb(0, responseCode, 0);
                    System.arraycopy(responseCode, 0, responsePayload, 0, 1);

                    ByteOrder.int2leb(responseData.length, sizeVal, 0);
                    System.arraycopy(sizeVal, 0, responsePayload, 1, 2);

                    System.arraycopy(responseData, 0, responsePayload, 3, responseData.length);
                    return responsePayload;
                }else{
                    byte[] responsePayload = new byte[1];
                    ByteOrder.int2leb(1, responseCode, 0);
                    System.arraycopy(responseCode, 0, responsePayload, 0, 1);
                    return responsePayload;
                }
            case 3:
                System.out.println("Command:" + "Remove");
                responseData = operateWithHashMap(keyBytes, null, 2);
                if(responseData == null){
                    byte[] responsePayload = new byte[1];
                    ByteOrder.int2leb(1, responseCode, 0);
                    System.arraycopy(responseCode, 0, responsePayload, 0, 1);
                    //System.out.println("Status Coqde: " + responsePayload[0]);
                    return responsePayload;
                }
                break;
            case 4:
                System.out.println("Command:" + "Shut Down");
                System.exit(0);
                break;
            case 5:
                System.out.println("Command:" + "Delete All");
                operateWithHashMap(null, null, 4);
                break;
            default:
                System.out.println("Command:" + "Unknown");
                byte[] responsePayload = new byte[1];
                ByteOrder.int2leb(5, responseCode, 0);
                System.arraycopy(responseCode, 0, responsePayload, 0, 1);
                //System.out.println("Status Code: " + responsePayload[0]);
                return responsePayload;
        }

        ByteOrder.int2leb(0,responseCode,0);
        //System.out.println("Status Code: " + responseCode[0]);
        return responseCode;
    }
}
