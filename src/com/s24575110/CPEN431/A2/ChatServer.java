package com.s24575110.CPEN431.A2;
import java.net.*;
import java.util.*;
import com.matei.eece411.util.*;
/**
 * Created by KangShiang on 2016-01-12.
 * This class implements the protocol level of the application.
 */
public class ChatServer{
    private final int headerSize = 16;
    private final int packageSize = 16400;
    private int sPort = 5110;

    private DatagramSocket udpSocket;
    private byte[] rData;
    //private byte[] sData;
    private static HashMap rlogs = new HashMap();

    /**
     * This method configures the connection.
     * @exception SocketException if the socket could not be opened,
     *              or the socket could not bind to the specified local port.
     */
    public void configConn() throws Exception{
        udpSocket = new DatagramSocket(sPort);
        rData = new byte[packageSize];
        //sData = new byte[packageSize];
    }


    /**
     * This method simply stores the value in the hashmap given a key.
     * @param key key to the hashmap.
     * @param val val to be stored.
     * @return the val that was stored.
     */

    private static byte[] storeValueWithKey(String key, byte[] val){
        byte[] data = (byte[])rlogs.put(key, val);
        return data;
    }

    /**
     * This method simply stores the value in the hashmap given a key with
     * one guarantee: No duplicate!
     * @param key key to the hashmap.
     * @param val val to be stored.
     * @return the val that was stored.
     */
    private static byte[] storeValueWithKeyNoDup(byte[] key, byte[] val){
        String hexKey = StringUtils.byteArrayToHexString(key);
        byte[] value = (byte[])rlogs.get(hexKey);
        if(value == null){
            byte[] data = storeValueWithKey(hexKey, val);
            return data;
        }
        return null;
    }

    /**
     * This method simply update the value in the hashmap given a key.
     * @param key key to the hashmap.
     * @param val val to be stored.
     * @return the val that was updated.
     */
    private static byte[] putValueWithKey(byte[] key, byte[] val){
        String hexKey = StringUtils.byteArrayToHexString(key);
        byte[] data = (byte[])storeValueWithKey(hexKey,val);
        return data;
    }

    /**
     * This method deletes value from the hashmap given a key.
     * @param key key to the value that is going to be deleted.
     * @return the val data that was deleted.
     */
    private static byte[] deleteValueWithKey(byte[] key){
        String hexKey = StringUtils.byteArrayToHexString(key);
        byte[] data = (byte[])rlogs.remove(hexKey);
        return data;
    }

    /**
     * This method gets value from the hashmap given a key.
     * @param key key to the value that is going to be retrieved.
     * @return the val data that was retrieved.
     */
    private static byte[] getValueWithKey(byte[] key){
        String hexKey = StringUtils.byteArrayToHexString(key);
        byte[] data = (byte[])rlogs.get(hexKey);
        return data;
    }

    /**
     * This method perform transaction on rlogs. Atomicity is
     * guaranteed within this function.
     * @param mode mode of operation:
     *             0 : stores the value given a key and makes sure
     *                 duplicate does not exist
     *             1 : update the value for a given key. If the key
     *                 does not exists, it creates a new entry with the key
     *             2 : deletes the value for a given key. It returns null
     *                 if the key does not exist.
     *             3 : get the value given key.
     * @param key key to the hashmap.
     * @param val val to be stored.
     * @return the val data that the operation was performed on.
     */
    public static synchronized byte[] operateWithHashMap(byte[] key, byte[] val, int mode){
        byte[] data;
        switch (mode) {
            case 0:
                data = storeValueWithKeyNoDup(key, val);
                return data;
            case 1:
                data = putValueWithKey(key, val);
                return data;
            case 2:
                data = deleteValueWithKey(key);
                return data;
            case 3:
                data = getValueWithKey(key);
                return data;
            default:
                break;
        }
        data = null;
        return data;
    }

    /**
     * This method parses the packet and extracts the protocol unique ID
     * @param packet UDP packet that is being processed
     * @return the Unique Identifier in the form of byte array.
     */
    private byte[] parseID(byte[] packet) throws Exception{
        byte[] header = new byte[headerSize];
        System.arraycopy(packet,0,header,0,headerSize);
        return header;
    }

    /**
    * This method waits until a datagram is received. Once the
    * datagram is received, it calls {@code handlePacket} to handle
    * the datagram.
    */
    public void receivePacket() throws Exception{
        DatagramPacket receivePacket = new DatagramPacket(rData, rData.length);
        udpSocket.receive(receivePacket);
        try {
            new Thread(new PacketManager(receivePacket, udpSocket)).start();
        }catch(OutOfMemoryError e){
            System.out.println(e.toString());
            byte[] uniqueID = parseID(receivePacket.getData());
            byte[] responseCode = new byte[4];
            byte[] responsePayload = new byte[1];

            ByteOrder.int2leb(3, responseCode, 0);
            System.arraycopy(responseCode, 0, responsePayload, 0, 1);

            byte[] rPacket = new byte[headerSize + responsePayload.length];
            System.arraycopy(uniqueID, 0, rPacket, 0, headerSize);
            System.arraycopy(responsePayload,0,rPacket,headerSize,responsePayload.length);

            InetAddress IPAddress = receivePacket.getAddress();
            int port = receivePacket.getPort();
            DatagramPacket sendPacket = new DatagramPacket(rPacket, rPacket.length, IPAddress, port);
            udpSocket.send(sendPacket);
        }
    }

    /**
     * Constructor method: configures the socket and calls
     * {@code receivePacket} to wait for incoming datagram.
     */
    public ChatServer(String Port) throws Exception{
        sPort = Integer.parseInt(Port);
        configConn();
        System.out.println("INFO: - Starting Server");
        while (true) {
            receivePacket();
        }
    }
}
